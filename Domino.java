package base;

public class Domino implements Comparable<Domino> {
    public int high;
    public int low;
    public int hx;
    public int hy;
    public int lx;
    public int ly;
    public boolean placed = false;

    public Domino(int high, int low) {
        this.high = high;
        this.low = low;
    }

    public void place(int hx, int hy, int lx, int ly) {
        this.hx = hx;
        this.hy = hy;
        this.lx = lx;
        this.ly = ly;
        placed = true;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        appendDominoValues(result);
        if (!placed) {
            result.append("unplaced");
        } else {
            appendPlacementCoordinates(result, hx + 1, hy + 1);
            appendPlacementCoordinates(result, lx + 1, ly + 1);
        }
        return result.toString();
    }

    private void appendDominoValues(StringBuilder result) {
        result.append("[");
        result.append(high);
        result.append(low);
        result.append("]");
    }

    private void appendPlacementCoordinates(StringBuilder result, int x, int y) {
        result.append("(");
        result.append(x);
        result.append(",");
        result.append(y);
        result.append(")");
    }

    public void invert() {
        swapCoordinates(hx, lx);
        swapCoordinates(hy, ly);
    }

    private void swapCoordinates(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    public boolean ishl() {
        return hy == ly;
    }

    public int compareTo(Domino other) {
        if (this.high < other.high) {
            return 1;
        }
        return this.low - other.low;
    }
}
