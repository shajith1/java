package base;

import java.net.InetAddress;

/**
 * Manages the connection and initiation of a game.
 * 
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */
public class ConnectionGenius {

    private InetAddress ipa;

    /**
     * Constructs a ConnectionGenius with the given IP address.
     * 
     * @param ipAddress The IP address for the connection.
     */
    public ConnectionGenius(InetAddress ipAddress) {
        this.ipa = ipAddress;
    }

    /**
     * Initiates the game by performing necessary steps.
     */
    public void fireUpGame() {
        downloadWebVersion();
        connectToWebService();
        prepareToPlay();
    }

    /**
     * Downloads the specialized web version for the game.
     */
    private void downloadWebVersion() {
        System.out.println("Getting specialized web version.");
        System.out.println("Wait a couple of moments");
    }

    /**
     * Connects to the web service required for the game.
     */
    private void connectToWebService() {
        System.out.println("Connecting");
    }

    /**
     * Signals that the system is ready to play the game.
     */
    private void prepareToPlay() {
        System.out.println("Ready to play");
    }
}
