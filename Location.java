package base;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Location extends SpacePlace {
    private Coordinates coordinates;
    private DIRECTION direction;

    public enum DIRECTION {
        VERTICAL, HORIZONTAL
    }

    public Location(int row, int col) {
        this.coordinates = new Coordinates(row, col);
    }

    public Location(int row, int col, DIRECTION direction) {
        this(row, col);
        this.direction = direction;
    }

    public String toString() {
        return (direction == null)
                ? "(" + (coordinates.getCol() + 1) + "," + (coordinates.getRow() + 1) + ")"
                : "(" + (coordinates.getCol() + 1) + "," + (coordinates.getRow() + 1) + "," + direction + ")";
    }

    public void drawGridLines(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        drawHorizontalLines(g);
        drawVerticalLines(g);
    }

    private void drawHorizontalLines(Graphics g) {
        for (int row = 0; row <= 7; row++) {
            int y = 20 + row * 20;
            g.drawLine(20, y, 180, y);
        }
    }

    private void drawVerticalLines(Graphics g) {
        for (int col = 0; col <= 8; col++) {
            int x = 20 + col * 20;
            g.drawLine(x, 20, x, 160);
        }
    }

    public static int getInt() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                return Integer.parseInt(reader.readLine());
            } catch (IOException | NumberFormatException e) {
            }
        } while (true);
    }

    private static class Coordinates {
        private int row;
        private int col;

        public Coordinates(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }
    }
}
