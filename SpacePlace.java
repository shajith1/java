package base;

/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class SpacePlace {
    private Coordinates coordinates;
  
    public SpacePlace() {
        coordinates = new Coordinates(0, 0);
    }

    public SpacePlace(double theta, double phi) {
        coordinates = new Coordinates(0, 0, theta, phi);
    }

    public int getxOrg() {
        return coordinates.getxOrg();
    }

    public void setxOrg(int xOrg) {
        coordinates.setxOrg(xOrg);
    }

    public int getyOrg() {
        return coordinates.getyOrg();
    }

    public void setyOrg(int yOrg) {
        coordinates.setyOrg(yOrg);
    }

    public double getTheta() {
        return coordinates.getTheta();
    }

    public void setTheta(double theta) {
        coordinates.setTheta(theta);
    }

    public double getPhi() {
        return coordinates.getPhi();
    }

    public void setPhi(double phi) {
        coordinates.setPhi(phi);
    }

    private class Coordinates {
        private int xOrg;
        private int yOrg;
        private double theta;
        private double phi;

        public Coordinates(int xOrg, int yOrg) {
            this.xOrg = xOrg;
            this.yOrg = yOrg;
        }

        public Coordinates(int xOrg, int yOrg, double theta, double phi) {
            this(xOrg, yOrg);
            this.theta = theta;
            this.phi = phi;
        }

        public int getxOrg() {
            return xOrg;
        }

        public void setxOrg(int xOrg) {
            this.xOrg = xOrg;
        }

        public int getyOrg() {
            return yOrg;
        }

        public void setyOrg(int yOrg) {
            this.yOrg = yOrg;
        }

        public double getTheta() {
            return theta;
        }

        public void setTheta(double theta) {
            this.theta = theta;
        }

        public double getPhi() {
            return phi;
        }

        public void setPhi(double phi) {
            this.phi = phi;
        }
    }
}
